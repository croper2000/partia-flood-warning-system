### Uses uses geo.stations_by_distance and prints 
# 10 closest/furthest stations to cambridge"""

from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list


def run():
    """Requirements for Task 1B"""

    # cambridge city centre
    CCC_coord = (52.2053, 0.1218)
    # Build list of stations
    stations = build_station_list()
    # get a list of (station,distance) tuples using stations_by_distance
    initial_list = stations_by_distance(stations,CCC_coord)
    # create a list of (station name, town, distance) tuples
    final_list = []
    for i in initial_list:       # i is a (station, distance) tuple
        ith_tuple = (i[0].name, i[0].town, i[1])
        final_list.append(ith_tuple)


    print ('_______')
    print ("The ten closest stations to Cambridge")
    print (final_list[:10])
    print ('_______')
    print ("The ten furthest stations from Cambridge")
    print (final_list[-10:])

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()