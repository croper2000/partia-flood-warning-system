from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels, build_fake_station_list

def test_stations_level_over_threshold():

    # Build a fake list of stations
    fakestations = build_fake_station_list()
    #make list of station objects with relative level >0.8
    inital_list2 = stations_level_over_threshold(fakestations,0.8)
    assert len(inital_list2) ==2
    assert inital_list2[0][1]>inital_list2[1][1]

    # from the fake stations, two should appear in this list, in descending order

def test_stations_highest_rel_level():
    # Build a fake list of stations
    fakestations = build_fake_station_list()
    Highestlevellist = stations_highest_rel_level(fakestations, 1)
    assert len(Highestlevellist)==1
    assert Highestlevellist[0].name == "Hobbiton"

    
    
    


    





