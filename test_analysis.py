# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the analysis module"""
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import datetime

from floodsystem.analysis import plot_water_level_with_fit, polyfit
from floodsystem.stationdata import build_station_list


def test_polyfit():

    x=[datetime.datetime(2000,1,1), datetime.datetime(2000,1,2), datetime.datetime(2000,1,3), datetime.datetime(2000,1,4)]
    y1=[1,4,9,16]
    y2=[2,5,10,17]
    p1=polyfit(x,y1,3)[0]
    p2=polyfit(x,y2,3)[0]

    assert p1.c[-1]-1<0.00001
    assert p2.c[-1]-2<0.00001

def test_plot_water_level_with_fit():
    stations=build_station_list()
    x=[datetime.datetime(2019,1,1), datetime.datetime(2019,1,2), datetime.datetime(2019,1,3), datetime.datetime(2019,1,4)]
    y1=[1,4,9,16]
    y2=[2,5,10,17]
    plot_water_level_with_fit(stations[0],x,y1,3)
    plot_water_level_with_fit(stations[0],x,y2,3)

    assert True

    
