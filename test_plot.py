# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the plot module"""
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import datetime

from floodsystem.plot import plot_water_levels
from floodsystem.stationdata import build_station_list

def test_plot_water_levels():
    stations=build_station_list()
    x=[datetime.datetime(2000,1,1), datetime.datetime(2000,1,2), datetime.datetime(2000,1,3), datetime.datetime(2000,1,4)]
    y1=[1,4,9,16]
    y2=[2,5,10,17]
    plot_water_levels(stations[0],x,y1)
    plot_water_levels(stations[0],x,y2)

    assert True
