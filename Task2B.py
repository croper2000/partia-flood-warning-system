from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels, build_fake_station_list



def run():
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    #get list of stations with relative level above 0.8
    inital_list = stations_level_over_threshold(stations,0.8)
    #print (inital_list)
    for i in inital_list:
        print (i[0].name +" "+ str(i[1]))
        #print (i[1])

    
    

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()