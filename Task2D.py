# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT

from datetime import datetime, timedelta

from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list


def run():

    # Build list of stations
    stations = build_station_list()

    # Station name to find
    station_name = "Cam"

    # Find station
    station_cam = stations[0]
    for i in range(len(stations)):
        if stations[i].name == station_name:
            station_cam = stations[i]
            break

    # Check that station could be found. Return if not found.
    if not station_cam:
        print("Station {} could not be found".format(station_name))
        return

    # Alternative find station 'Cam' using the Python 'next' function
    # (https://docs.python.org/3/library/functions.html#next). Raises
    # an exception if station is not found.
    try:
        station_cam = next(s for s in stations if s.name == station_name)
    except StopIteration:
        print("Station {} could not be found".format(station_name))
        return

    # Fetch data over past 2 days

    fetchedData=fetch_measure_levels(station_cam.measure_id, timedelta(2))
    dates = fetchedData[0]
    levels = fetchedData[1]

    # Print level history
    for date, level in zip(dates, levels):
        print(str(date), level)


if __name__ == "__main__":
    print("*** Task 2D: CUED Part IA Flood Warning System ***")
    run()
