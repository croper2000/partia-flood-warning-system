# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT

from datetime import datetime
from datetime import timedelta

from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.datafetcher import fetch_measure_levels


def run():
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)
    
    
    Highestlevellist = stations_highest_rel_level(stations, 5)
    for i in Highestlevellist:
        fetchedData=fetch_measure_levels(i.measure_id, timedelta(10))
        plot_water_levels(i, fetchedData[0], fetchedData[1])
    
    


if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()
