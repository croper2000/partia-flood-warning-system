###task 1F

from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1F"""
    #builds a list of all stations with inconsistent typical range data
    stations = build_station_list()
    inconsistent_stations = inconsistent_typical_range_stations(stations)
    #Print a list of station names, in alphabetical order, for stations with inconsistent data.
    alphabeticallist = []
    for i in inconsistent_stations:
        alphabeticallist.append(i.name)
    alphabeticallist.sort()
    
    print(alphabeticallist)
    


if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()