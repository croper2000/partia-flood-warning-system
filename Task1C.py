###task 1C

from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1C"""
    # cambridge city centre
    CCC_coord = (52.2053, 0.1218)
    # Build list of stations
    stations = build_station_list()

    #list of stations(objects) within radius
    initial_list = stations_within_radius(stations,CCC_coord,10)
    #create list of just the station names, print
    final_list = []
    for i in initial_list:
        final_list.append(i.name)
    final_list.sort()
    print (final_list)
    


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()