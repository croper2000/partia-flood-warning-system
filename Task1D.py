###task 1D

from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1D"""
    # Build list of stations
    stations = build_station_list()
    # Build set of alphabetically sorted rivers
    riversSet=sorted(rivers_with_station(stations))
    # Build a list of the first 10 rivers
    riversList=[]
    print(len(riversSet))
    for i in range(10):
        riversList.append(riversSet[i])
    print(riversList)
    # Build dictionary of rivers and stations
    printDict=stations_by_river(stations)
    # Build a list for outputing station names
    stationNameList=[]
    # Build lists of the stations from the Task 1D assignment page
    aireList=['Airmyn', 'Apperley Bridge', 'Armley', 'Beal Weir Bridge', 'Bingley', 'Birkin Holme Washlands', 'Carlton Bridge', 'Castleford', 'Chapel Haddlesey', 'Cononley', 'Cottingley Bridge', 'Ferrybridge Lock', 'Fleet Weir', 'Gargrave', 'Kildwick', 'Kirkstall Abbey', 'Knottingley Lock', 'Leeds Crown Point', 'Saltaire', 'Snaygill', 'Stockbridge']
    camList=['Cam', 'Cambridge', 'Cambridge Baits Bite', 'Cambridge Jesus Lock', 'Dernford', 'Weston Bampfylde']
    thamesList=['Abingdon Lock', 'Bell Weir', 'Benson Lock', 'Boulters Lock', 'Bray Lock', 'Buscot Lock', 'Caversham Lock', 'Chertsey Lock', 'Cleeve Lock', 'Clifton Lock', 'Cookham Lock', 'Cricklade', 'Culham Lock', 'Days Lock', 'Ewen', 'Eynsham Lock', 'Farmoor', 'Godstow Lock', 'Goring Lock', 'Grafton Lock', 'Hannington Bridge', 'Hurley Lock', 'Iffley Lock', 'Kings Lock', 'Kingston', 'Maidenhead', 'Mapledurham Lock', 'Marlow Lock', 'Marsh Lock', 'Molesey Lock', 'Northmoor Lock', 'Old Windsor Lock', 'Osney Lock', 'Penton Hook', 'Pinkhill Lock', 'Radcot Lock', 'Reading', 'Romney Lock', 'Rushey Lock', 'Sandford-on-Thames', 'Shepperton Lock', 'Shifford Lock', 'Shiplake Lock', 'Somerford Keynes', 'Sonning Lock', 'St Johns Lock', 'Staines', 'Sunbury  Lock', 'Sutton Courtenay', 'Teddington Lock', 'Thames Ditton Island', 'Trowlock Island', 'Walton', 'Whitchurch Lock', 'Windsor Park']

    for i in printDict['Aire']:
        stationNameList.append(i.name)
        print(i.river)
    stationNameList.sort()
    print(stationNameList)
    print(len(stationNameList))
    print(len(aireList))
    stationNameList=[]
    for i in printDict['Cam']:
        stationNameList.append(i.name)
        print(i.river)
    stationNameList.sort()
    print(stationNameList)
    print(len(stationNameList))
    print(len(camList))
    stationNameList=[]
    for i in printDict['Thames']:
        stationNameList.append(i.name)
        print(i.river)
    stationNameList.sort()
    print(stationNameList)
    print(len(stationNameList))
    print(len(thamesList))


if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()