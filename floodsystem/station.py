# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""
#from .stationdata import update_water_levels


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None



    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d

    def typical_range_consistent(self):
        """returns true (only) if typical range is avaiable and consistent"""
        if self.typical_range == None:
            return False
        elif self.typical_range[1]-self.typical_range[0]<= 0:
            return False
        else:
            return True

    def relative_water_level(self):
        """returns the latest water level as a fraction of the typical range"""
        if self.latest_level ==  None:
            return None
        elif self.typical_range==  None:
            return None
        else:
            typicalrange = self.typical_range[1]-self.typical_range[0]
            ratio = (self.latest_level-self.typical_range[0])/typicalrange
            return ratio


def inconsistent_typical_range_stations(stations):
    """given a list of station objects, returns a list of station objects that have inconsistent data"""
    #make list of inconsistent stations
    inconsistent_list = []
    #if station is inconsistent, append to list
    for i in stations:
        if MonitoringStation.typical_range_consistent(i)==False:
            inconsistent_list.append(i)
    return inconsistent_list
