# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
# haversine works out distance between two points on the globe from lat/long
# we may need to import harvesine before this runs? in a terminal use:
#  pip install haversine

from haversine import haversine, Unit

# inital testing with haversine
#lyon = (45.7597, 4.8422) # (lat, lon)
#paris = (48.8567, 2.3508)
#print (haversine(lyon, paris))
#print (haversine(lyon, paris, unit=Unit.MILES))



from .utils import sorted_by_key # noqa



def stations_by_distance(stations, p):
    """ Given a list of station objects and a point p, 
    returns an ordered list of (station, distance) tuples. 
    Distance is from the point p  """
    #create empty list
    stn_dist_list = []
    
    for i in stations:
        # make a tuple of (station, distance)
        ithtuple = (i,haversine(i.coord,p))
        #append this to the list
        stn_dist_list.append(ithtuple)
 
    #sort this list and return
    sortedlist = sorted_by_key(stn_dist_list,1)
    return sortedlist



def stations_within_radius(stations, centre, r):
    """ given a list of Monitoring Station objects, gives a list of
    stations within radius of a central coordinate"""
    #create an empty list
    initial_list = []
    #loop through the list of stations, if distance<= r then append to list
    for i in stations:
        if haversine(i.coord,centre) < r:
            initial_list.append(i)
    return initial_list



def rivers_with_station(stations):
    """  given a list of station objects, returns a container 
    with the names of the rivers with a monitoring station """
    # create a set that we return (sets don't have repeats)
    returnRivers=set()
    # create condition if we add element to the set
    condition=True
    # loop through stations list and add rivers to set
    for i in stations:
        addStatement=i.river
        condition=True
        # check if river stars with River
        if addStatement[:5] == 'River':
            # cut out the word river
            addStatement=addStatement[6:]
        if condition == True:
            returnRivers.add(addStatement)
    return returnRivers

def stations_by_river(stations):
    """ implement a function that returns a Python dict that maps 
    river names to a list of station objects on a given river """
    # create a list of the rivers which will be the keys for the dictionary
    riverList=rivers_with_station(stations)
    # create return dictionary
    returnDict=dict()
    # create a list that will be filled with station objects
    mappedStationList=[]
    # nested loops that create lists of stations for each river
    # and add entries to the dictionary of the river:stationList
    for i in riverList:
        mappedStationList=[]
        for j in stations:
            riverHolder=j.river
            if riverHolder[:5] == 'River':
                riverHolder=riverHolder[6:]
            # extra space to make sure the word is found not just the string of chars
            if i == riverHolder:
                mappedStationList.append(j)
        returnDict[i]=mappedStationList
    return returnDict

def rivers_by_station_number(stations, N):
    """ implement a function in geo that determines the N rivers
    with the greatest number of monitoring stations """
    # create a dictionary of rivers to station objects
    dictStations=stations_by_river(stations)
    # create a list of tuples of the rivers with the most stations
    thisRiverList=list()
    # create a string that holds the river name with the most stations
    highKey=""
    # create a last max value that holds the last highest value
    lastMax=0
    # create a conditional boolean which shuts down while loop
    conditional=True
    # create a counter
    counter=0
    # while loop that controls how many times we find the river with the most stations
    while conditional:
        highKey=""
        # adds one to counter
        counter+=1
        # runs through all the keys and finds the key that refers to the longest list of stations
        for i in dictStations.keys():
            # set the high string at the begining of the run throug
            if highKey == '':
                highKey=i
            # if a key refers to a list with more stations than the current high key refers to then this sets the new high key
            elif len(dictStations[i]) > len(dictStations[highKey]):
                highKey=i
        # failsafe to shutdown program
        if counter > N+1:
            break
        # checks if the N+1 is equal to the N and will continue the program if so
        if counter == N+1:
            if lastMax == len(dictStations[highKey]):
                N+=1
            else:
                break
        # removes the refrence with the most items from the dictionary, and creates a tuple, which is added to the list
        lastMax=len(dictStations.pop(highKey))
        thisRiverList.append(tuple([highKey, lastMax]))
    return thisRiverList