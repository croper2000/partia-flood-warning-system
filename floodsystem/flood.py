


from .utils import sorted_by_key

from .station import MonitoringStation

def stations_level_over_threshold(stations, tol):
    """ From a list of station objects, and relative water level tolerance, 
    creates a list of (station object, relative water level ratio) tuples
    for which the relative water level ratio exceeds the tolerance """
    tuple_list = []
    #iterate through stations,if ratio>tol then append to list
    for i in stations:
        if MonitoringStation.relative_water_level(i)!=None: #ignores inconsistent data
            if MonitoringStation.relative_water_level(i)>tol:
                #create a (station object, relative water level) tuple and append
                ith_tuple = (i,MonitoringStation.relative_water_level(i))
                tuple_list.append(ith_tuple)
    sortedlist = sorted_by_key(tuple_list, 1, reverse=True)
    return sortedlist

def stations_highest_rel_level(stations, N):
    """ returns a list of the N stations (objects) 
    at which the water level, relative to the typical range, is highest"""
    inital_list = []
    for i in stations:
        if MonitoringStation.relative_water_level(i)!=None: #ignores inconsistent data
            #order a list of stations based on their relative level
            inital_list.append(i)
            inital_list.sort(key=lambda x: x.relative_water_level(), reverse=True)
    final_list = inital_list[:N]
    return final_list

    







