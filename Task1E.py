###task 1E

from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1E"""
    # Build list of stations
    stations = build_station_list()
    # Build list of rivers with the most stations
    riversList1=rivers_by_station_number(stations, 14)
    print(riversList1)
    riversList2=rivers_by_station_number(stations, 6)
    print(riversList2)
    riversList3=rivers_by_station_number(stations, 7)
    print(riversList3)
    riversList4=rivers_by_station_number(stations, 8)
    print(riversList4)
    


if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    run()