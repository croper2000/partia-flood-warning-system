# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT

from datetime import datetime
from datetime import timedelta

from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.analysis import plot_water_level_with_fit, polyfit


def run():
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)
    
    stationsAboveThreshold = stations_level_over_threshold(stations,1.25)
    score=0 #score
    townsScore={}

    for i in stationsAboveThreshold:
        score=0
        fetchedData=fetch_measure_levels(i[0].measure_id, timedelta(10))
        if fetchedData[1][-1]-fetchedData[1][-100]>0: # checks if rising over last day or so
            score+=1
        if fetchedData[1][-1]-fetchedData[1][-5]>0: # checks if rising over last day or so
            score+=1
        score+=int((i[1]-1)*2) # looks at relative height
        score+=int((i[1]-1)*i[0].typical_range[1]*0.5) # looks at actual height
        if i[0].town not in townsScore.keys(): # updates a dictionary of town occurances score
            townsScore[i[0].town]=score
        else:
            if townsScore[i[0].town]<score:
                townsScore[i[0].town]=score

    
    for j in townsScore.keys(): #score convert to levels
        if townsScore[j]>20: #this corresponds with online
            print(str(j)+": Severe Flood Warning")
        elif townsScore[j]>7:
            print(str(j)+": High Flood Warning")
        elif townsScore[j]>2:
            print(str(j)+": Moderate Flood Warning")
        else:
            print(str(j)+": Low Flood Warning")
    


if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()
