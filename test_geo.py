"""Unit test for the geo module"""

import haversine

from floodsystem.geo import stations_by_distance 
from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def test_stations_by_distance():
    # test using cambridge city centre
    CCC_coord = (52.2053, 0.1218)
    # Build list of stations
    stations = build_station_list()
    # get a list of (station,distance) tuples using stations_by_distance
    list_of_tuples = stations_by_distance(stations,CCC_coord)

    # assert list_of_tuples is same length as stations 
    assert len(list_of_tuples)==len(stations)

    # assert station 'Cambridge Jesus Lock' is in the list
    for i in list_of_tuples:
        if i[0].name == 'Cambridge Jesus Lock':
            station_CJL = i
            break

    # Assert that station is found
    assert station_CJL
    # Assert that distance to Cam is <<2km
    assert station_CJL[1]< 2



def test_stations_within_radius():
    # test using cambridge city centre
    CCC_coord = (52.2053, 0.1218)
    # Build list of stations
    stations = build_station_list()
    # get a list of stations within a 10km radius of Cambridge
    list_of_stations = stations_within_radius(stations,CCC_coord,10)

    # assert list_of_stations has 11 stations in it 
    assert len(list_of_stations) == 11

    # assert station 'Cambridge Jesus Lock' is in the list
    for i in list_of_stations:
        if i.name == 'Cambridge Jesus Lock':
            station_CJL = i
            break

    # Assert that station is found
    assert station_CJL

    









